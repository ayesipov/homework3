package hw4.task2;

import java.util.ArrayList;

public class Dialog {
    private final ArrayList<Message> messages = new ArrayList<>();

    public void addMessages(Message message){
        messages.add(message);
    }

    public void printMessage(){
        System.out.println(messages);
    }

    @Override
    public String toString() {
        return "Dialog{" +
                "messages=" + messages +
                '}'+"\n";
    }
}
