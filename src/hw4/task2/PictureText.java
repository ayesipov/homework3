package hw4.task2;

public class PictureText implements TextType{

    private final String pictureText;

    @Override
    public String text() {
        return pictureText;
    }

    public PictureText(String pictureText) {
        this.pictureText = pictureText;
    }
}
