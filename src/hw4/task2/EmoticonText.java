package hw4.task2;

public class EmoticonText implements TextType {
    private final String emoticonText;

    @Override
    public String text() {
        return emoticonText;
    }

    public EmoticonText(String emoticonText) {
        this.emoticonText = emoticonText;
    }

    @Override
    public String toString() {
        return "EmoticonText{" +
                "emoticonText='" + emoticonText + '\'' +
                '}';
    }
}
