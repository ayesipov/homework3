package hw4.task2;

import java.time.LocalDateTime;

public class Message {
    private final String author;
    private final LocalDateTime postingTime;
    private final TextType text;

    public Message(String author, LocalDateTime postingTime, TextType text) {
        this.author = author;
        this.postingTime = postingTime;
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public LocalDateTime getPostingTime() {
        return postingTime;
    }

    public TextType getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "author='" + author + '\'' +
                ", postingTime=" + postingTime +
                ", text=" + text +
                '}'+"\n";
    }
}
