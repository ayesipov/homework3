package hw4.task2;

public class PlainText implements TextType {

    private final String plainText;

    @Override
    public String text() {
        return plainText;
    }

    public PlainText(String plainText) {
        this.plainText = plainText;
    }

    @Override
    public String toString() {
        return "PlainText{" +
                "plainText='" + plainText + '\'' +
                '}';
    }
}
