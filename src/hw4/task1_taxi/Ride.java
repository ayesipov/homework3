package hw4.task1_taxi;

public class Ride {
    private final int passengers;
    private final int distance;
    private final int duration;
    private final Tariff tariff;

    public Ride(int passengers, int distance, int duration, Tariff tariff) {
        if (passengers<0){
            throw new IllegalArgumentException("passengers can not be <0");
        }
        if (distance<0){
            throw new IllegalArgumentException("distance can not be <=0");
        }
        if (duration<0){
            throw new IllegalArgumentException("duration can not be <0");
        }
        this.passengers = passengers;
        this.distance = distance;
        this.duration = duration;
        this.tariff = tariff;
    }

    public int getPassengers() {
        return passengers;
    }

    public int getDistance() {
        return distance;
    }

    public int getDuration() {
        return duration;
    }

    public long getPrice(){
        return tariff.calculatePrice(this);
    }
}
