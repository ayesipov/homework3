package hw4.task1_taxi;

public class TaxiRunner {
    public static void main(String[] args) {

        Tariff family = new FamilyTariff();
        Tariff standard = new StandardTariff();

        Ride rideFamily = new Ride(4,7,28,family);
        Ride rideStandard = new Ride(2,12,49,standard);

        RidesHistory history = new RidesHistory();
        history.addRide(rideFamily);
        history.addRide(rideStandard);


        System.out.println("Total ride's price = " +history.getTotalPrice()); //Total ride's price = 273
    }
}
