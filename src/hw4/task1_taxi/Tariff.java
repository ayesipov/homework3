package hw4.task1_taxi;

public interface Tariff {
    long calculatePrice(Ride ride);
}

