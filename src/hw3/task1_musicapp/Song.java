package hw3.task1_musicapp;

public class Song {
    private final String title;
    private final String author;
    private final String genre;
    private final int length;
    private final AudioContent content;

    public Song(String title, String author, String genre, int length, AudioContent content) {
        if (title.isEmpty()){
            throw new IllegalArgumentException("\"title\" can not be empty");
        }
        if (author.isEmpty()){
            throw new IllegalArgumentException("\"author\" can not be empty");
        }
        if (genre.isEmpty()){
            throw new IllegalArgumentException("\"genre\" can not be empty");
        }
        if (length<=0){
            throw new IllegalArgumentException("\"length\" can not be <= 0");
        }
        this.author = author;
        this.content = content;
        this.genre = genre;
        this.length = length;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getGenre() {
        return genre;
    }

    public int getLength() {
        return length;
    }

    public AudioContent getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", length=" + length +
                ", content=" + content +
                '}'+"\n";
    }
}
