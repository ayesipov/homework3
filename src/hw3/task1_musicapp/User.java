package hw3.task1_musicapp;

import java.util.ArrayList;

public class User {
    private final String name;
    private final ArrayList<Playlist> playlists = new ArrayList<>();

    public User(String name) {
        if (name.isEmpty()){
            throw new IllegalArgumentException("User`s name can`t be empty");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Playlist createPlaylist(String name) {
        Playlist created = new Playlist(name);
        playlists.add(created);
        return created;
    }

    public Playlist findByTitle(String searchRequest) {
        if (searchRequest.isEmpty()){
            throw new IllegalArgumentException("\"Search request\" can`t be empty");
        }
        Playlist result = new Playlist(name);
        for (Playlist playlist : playlists) {
            if (playlist.getTitle().equalsIgnoreCase(searchRequest)) {
                result = playlist;
            }
        }
        return result;
    }

    public void addSongToPlaylist(String title, Song song) {
        if (title.isEmpty()){
            throw new IllegalArgumentException("\"Title\" can`t be empty");
        }
        for (Playlist playlist : playlists) {
            if (playlist.getTitle().equalsIgnoreCase(title)) {
                playlist.addSong(song);
            }
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", playlists=" + playlists +
                '}';
    }
}
