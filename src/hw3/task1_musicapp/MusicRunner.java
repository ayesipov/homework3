package hw3.task1_musicapp;

public class MusicRunner {
    public static void main(String[] args) {
        AudioContent boomerangMp3 = new AudioContent("Boomerang");
        AudioContent westCoastMp3 = new AudioContent("West Coast");
        AudioContent onlyMp3 = new AudioContent("Only");
        AudioContent machineMp3 = new AudioContent("Machine");
        AudioContent hitTheFloorMp3 = new AudioContent("Hit the floor");
        AudioContent faintMp3 = new AudioContent("Faint");
        AudioContent nobodysListening = new AudioContent("Nobody's listening");
        Song boomerang = new Song("Boomerang", "Imagine Dragons", "Indy", 3, boomerangMp3);
        Song westCoast = new Song("West Coast", "Imagine Dragons", "Indy", 3, westCoastMp3);
        Song only = new Song("Only","Imagine Dragons", "Indy", 4, onlyMp3);
        Song machine = new Song("Machine", "Imagine Dragons", "Indy",3, machineMp3);
        Song hitTheFloor = new Song("Hit the Floor", "Linkin Park", "Rock", 3, hitTheFloorMp3);
        Song faint = new Song("Faint", "Linkin Park", "Rock",3, faintMp3);
        Song nobodyslistening = new Song ("Nobody's listening", "Linkin Park", "Rock", 5, nobodysListening);

        User firstUser = new User("First User");
        firstUser.createPlaylist("Indy");
        firstUser.addSongToPlaylist("Indy",boomerang);
        firstUser.addSongToPlaylist("Indy",westCoast);
        firstUser.addSongToPlaylist("Indy",only);
        firstUser.addSongToPlaylist("Indy",machine);

        firstUser.createPlaylist("Rock");
        firstUser.addSongToPlaylist("Rock",hitTheFloor);
        firstUser.addSongToPlaylist("Rock",faint);
        firstUser.addSongToPlaylist("Rock",nobodyslistening);

        System.out.println(firstUser);
        //User{name='First User', playlists=[Playlist{title='Indy', songs=[Song{title='Boomerang', author='Imagine Dragons', genre='Indy', length=3, content=AudioContent{content='Boomerang'}}
        //, Song{title='West Coast', author='Imagine Dragons', genre='Indy', length=3, content=AudioContent{content='West Coast'}}
        //, Song{title='Only', author='Imagine Dragons', genre='Indy', length=4, content=AudioContent{content='Only'}}
        //, Song{title='Machine', author='Imagine Dragons', genre='Indy', length=3, content=AudioContent{content='Machine'}}
        //]}
        //, Playlist{title='Rock', songs=[Song{title='Hit the Floor', author='Linkin Park', genre='Rock', length=3, content=AudioContent{content='Hit the floor'}}
        //, Song{title='Faint', author='Linkin Park', genre='Rock', length=3, content=AudioContent{content='Faint'}}
        //, Song{title='Nobody's listening', author='Linkin Park', genre='Rock', length=5, content=AudioContent{content='Nobody's listening'}}
        //]}
        //]}


        System.out.println(firstUser.findByTitle("Indy").filterByAuthor("Imagine Dragons"));
        // [Song{title='Boomerang', author='Imagine Dragons', genre='Indy', length=3, content=AudioContent{content='Boomerang'}}
        //, Song{title='West Coast', author='Imagine Dragons', genre='Indy', length=3, content=AudioContent{content='West Coast'}}
        //, Song{title='Only', author='Imagine Dragons', genre='Indy', length=4, content=AudioContent{content='Only'}}
        //, Song{title='Machine', author='Imagine Dragons', genre='Indy', length=3, content=AudioContent{content='Machine'}}
        //]

        System.out.println(firstUser.findByTitle("Rock").filterByTitlePart("F"));
        // [Song{title='Hit the Floor', author='Linkin Park', genre='Rock', length=3, content=AudioContent{content='Hit the floor'}}
        //, Song{title='Faint', author='Linkin Park', genre='Rock', length=3, content=AudioContent{content='Faint'}}
        //]

        System.out.println(firstUser.findByTitle("Rock").getTotalLength());  //11




    }
}
