package hw3.task1_musicapp;

import java.util.ArrayList;

public class Playlist {
    private final String title;

    private final ArrayList<Song> songs = new ArrayList<>();

    public Playlist(String title) {
        if (title.isEmpty()){
            throw new IllegalArgumentException("title can not be empty");
        }
        this.title = title;
    }

    public void addSong (Song song){
        songs.add(song);
    }

    public String getTitle() {
        return title;
    }

    public long getTotalLength (){
        if (songs.isEmpty()){
            throw new IllegalStateException("List is Empty");
        }
        long totalLength = 0;
        for (Song song : songs) {
            totalLength += song.getLength();
        }
        return totalLength;
    }

    public ArrayList<Song> filterByTitlePart(String titlePart) {
        ArrayList<Song> titlePartResult = new ArrayList<>();
        for (Song song : songs) {
            if (song.getTitle().contains(titlePart)) {
                titlePartResult.add(song);
            }
            if (titlePartResult.isEmpty()) {
                throw new IllegalArgumentException("There are no songs");

            }
        }
        return titlePartResult;
    }

    public ArrayList<Song> filterByAuthor(String authorSearch){
        ArrayList<Song> authorSearchResult = new ArrayList<>();
        for (Song song : songs) {
            if (song.getAuthor().contains(authorSearch)){
                authorSearchResult.add(song);
            }
            if (authorSearch.isEmpty()){
                throw new IllegalArgumentException("There are no authors");
            }
        }return authorSearchResult;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "title='" + title + '\'' +
                ", songs=" + songs +
                '}'+"\n";
    }
}
