package hw3.task1_musicapp;

public class AudioContent {
    private final String content;

    public AudioContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "AudioContent{" +
                "content='" + content + '\'' +
                '}';
    }
}
