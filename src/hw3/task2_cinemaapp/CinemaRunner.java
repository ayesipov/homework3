package hw3.task2_cinemaapp;


import java.time.LocalDateTime;

public class CinemaRunner{
    public static void main(String[] args) {
        Movie bigDaddy = new Movie("Big Daddy",1999,1.34,"Kind movie for all family");

        HallForSession bigHall = new HallForSession("Big Hall",5,5);

        MovieSession movieSession = new MovieSession(bigDaddy, bigHall, 70, LocalDateTime.of(2019,3,30,18,30));

        bigHall.bookingPlace(5,2);
        bigHall.bookingPlace(5,3);

        System.out.println("Movie what you chose: " + bigDaddy.getMovieTitle()); //Movie what you chose: Big Daddy
        System.out.println("Total sum of sold tickets = " + movieSession.countMoneyFromSoldTickets()); //Total sum of sold tickets = 140
        System.out.println("Total sum of booked seats = " + bigHall.countBoockedSeats()); //Total sum of booked seats = 2
        System.out.println("Total sum of left free seats = " + bigHall.sumFreeSeats()); //Total sum of left free seats = 23


    }
}
