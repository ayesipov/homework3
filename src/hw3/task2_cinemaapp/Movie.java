package hw3.task2_cinemaapp;

public class Movie {
    private final String movieTitle;
    private final int year;
    private final double duration;
    private final String description;

    public Movie(String movieTitle, int year, double duration, String description) {
        if (movieTitle.isEmpty()){
            throw new IllegalArgumentException("movie Title can not be empty");
        }
        if (year<=0){
            throw new IllegalArgumentException("year con not be 0");
        }
        if (description.isEmpty()){
            throw new IllegalArgumentException("description can not be empty");
        }
        this.movieTitle = movieTitle;
        this.year = year;
        this.duration = duration;
        this.description = description;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public int getYear() {
        return year;
    }

    public double getDuration() {
        return duration;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieTitle='" + movieTitle + '\'' +
                ", year=" + year +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                '}'+"\n";
    }
}
