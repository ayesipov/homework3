package hw3.task2_cinemaapp;

import java.util.Arrays;

public class HallForSession {
    private String hallName;
    private int seats;
    private int rows;
    private boolean[][] hallForSession;


    public HallForSession(String hallName, int seats, int rows) {
        this.hallName = hallName;
        this.seats = seats;
        this.rows = rows;
        this.hallForSession = new boolean[seats][rows];
    }

    public void bookingPlace(int seatsForBooking, int rowForBooking){
        if (seatsForBooking > seats || rowForBooking > rows){
            throw new IllegalArgumentException("You cant book more than total value of the seats");
        }
        if(!hallForSession[seatsForBooking-1][rowForBooking-1]){
            hallForSession[seatsForBooking-1][rowForBooking-1] = true;
        }else{
            throw new IllegalStateException("seat each you chose "+seatsForBooking+" "+rowForBooking+" is already booked");
        }
    }

    public int countBoockedSeats() {
        int boockedSeats = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < seats; j++) {
                if (hallForSession[i][j]) {
                    boockedSeats++;
                }
            }
        }return boockedSeats;
    }

    public int sumFreeSeats(){
        return (rows * seats) - countBoockedSeats();
    }

    @Override
    public String toString() {
        return "HallForSession{" +
                "hallName='" + hallName + '\'' +
                ", seats=" + seats +
                ", rows=" + rows +
                ", hallForSession=" + Arrays.toString(hallForSession) +
                '}'+"\n";
    }
}
