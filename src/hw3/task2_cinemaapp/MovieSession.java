package hw3.task2_cinemaapp;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MovieSession {
    private Movie movie;
    private HallForSession hallForSession;
    private int ticketPrice;
    private LocalDateTime ssesionTime;

    public MovieSession(Movie movie, HallForSession hallForSession, int ticketPrice, LocalDateTime ssesionTime) {
        this.movie = movie;
        this.hallForSession = hallForSession;
        this.ticketPrice = ticketPrice;
        this.ssesionTime = ssesionTime;
    }

    public Movie getMovie() {
        return movie;
    }

    public HallForSession getHallForSession() {
        return hallForSession;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public LocalDateTime getSsesionTime() {
        return ssesionTime;
    }

    public int countMoneyFromSoldTickets(){
        return ticketPrice * hallForSession.countBoockedSeats();
    }

    @Override
    public String toString() {
        return "MovieSession{" +
                "movie=" + movie +
                ", hallForSession=" + hallForSession +
                ", ticketPrice=" + ticketPrice +
                ", ssesionTime=" + ssesionTime +
                '}'+"\n";
    }
}
